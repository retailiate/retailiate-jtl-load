FROM registry.gitlab.com/freckles-io/freckles:builder_dev as builder

COPY frecklets /tmp/install/frecklets

RUN frecklecute -r /tmp/install/frecklets singer-pipeline-jtl --path /home/freckles/pipeline/jtl-load --working-dir /pipeline_state_dir

FROM debian:buster
RUN apt-get -y update && apt-get -y upgrade && apt-get -y install libpq-dev

RUN adduser --disabled-password --gecos '' freckles
RUN adduser freckles sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN mkdir /pipeline_state_dir
RUN chown -R freckles:freckles /pipeline_state_dir

USER freckles
WORKDIR /home/freckles

COPY --from=builder /home/freckles/.pyenv .pyenv
COPY --from=builder /home/freckles/pipeline/jtl-load pipeline/jtl-load

#VOLUME /pipeline_state_dir

CMD ["/home/freckles/pipeline/jtl-load/run-pipeline.sh"]

